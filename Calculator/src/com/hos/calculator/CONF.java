/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hos.calculator;

/**
 *
 * @author Sankalp
 */
public interface CONF {
    // Hours rule (8/70(252000 seconds for 70 Hours) or 7/60(216000 seconds for 60 Hours))
    public static final long HOS_RULE_8_BY_70_HOURS= 252000; // If HoS rule is 8 days 70 hours (seconds for 70 Hours)
    //public static final long HOS_RULE_HOURS= 216000; // If HoS rule is 7 days 60 hours (seconds for 60 Hours)
}
