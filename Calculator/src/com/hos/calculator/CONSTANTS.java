/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hos.calculator;

/**
 *
 * @author Sankalp
 */
public interface CONSTANTS {
    public static final long MIN_1_RESET_SECONDS = 60; 
    public static final long HRS_34_RESET_SECONDS = 34*60*60; // Second for 34 hours to reset hos week cycle
    public static final long MIN_30_BREAK_SECONDS = 30*60;  // Seconds for 30 min breaks
    public static final long HRS_10_BREAK_SECONDS = 10*60*60; // Seconds for 10 hr breaks
 }
