/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hos.calculator;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.*;

/**
 *
 * @author adashwant
 */
public class ProcessEngine {

    public JSONArray truncate34HrsResetCycle(JSONArray hosWeekCycle) {
        try {
            // First find the 34 hour reset cycle starting day and entire hos cycle data

            long duration = 0; // Variable to calculate consecutive OFF duty or SB duty duration

            int i, j; // Variables for loops

            JSONObject hosForDay; // Get hos for single day with all status for that day
            JSONArray hosStatusForDay; // Get hos status for single day
            JSONObject hosEvent; // single HoS event out of all hos events of single day

            JSONArray currentHosCycleData = new JSONArray(); // Hos array after detecting 34 hr break cycle

            // Loop to process one day HoS at a time
            // This loop will be in reverse order because we want to detect 34 Hrs resest cycle close to current day
            // Loop Started from hosWeekCycle.length()-1 because if array length is 4 then index varies from 0 to 3.
            for (i = hosWeekCycle.length() - 1; i >= 0; i--) {
                // Get HoS object of 'i'th day
                hosForDay = hosWeekCycle.getJSONObject(i);

                // Get array of HoS status for that day
                hosStatusForDay = hosForDay.getJSONArray("status");

                // Loop to process one status of the day at a time
                // This loop will be in reverse order because we want to detect 34 Hrs resest cycle close to current day
                // Loop Started from hosStatusForDay.length()-1 because if array length is 4 then index varies from 0 to 3.
                for (j = hosStatusForDay.length() - 1; j >= 0; j--) {
                    // Get 'j'th HoS status object for that day
                    hosEvent = hosStatusForDay.getJSONObject(j);

                    // Get consecutive OFF duty and SB status
                    if (hosEvent.getString("dutyStatus").equals("SLEEPER BERTH") || hosEvent.getString("dutyStatus").equals("OFF DUTY")) {
                        duration += hosEvent.getInt("duration");
                        System.out.println("Duration: " + duration);

                        if (duration >= CONSTANTS.HRS_34_RESET_SECONDS) {
                            // Break loop if duration exceeds 34 hrs seconds
                            break;
                        }
                    } else {
                        // Reset duration count to zero if Duty status is other than SLEEPER BERTH or OFF DUTY
                        duration = 0;
                        System.out.println("Duration reset");
                    }
                }

                if (duration >= CONSTANTS.HRS_34_RESET_SECONDS) {
                    break;
                } else {
                    currentHosCycleData.put(hosForDay);
                }
            }

            // Flush HoS data from hosWeekCycle array
            hosWeekCycle = new JSONArray("[]");
            // Loop Started from hosStatusForDay.length()-1 because if array length is 4 then index varies from 0 to 3.
            // We also want to remove HoS day having Partial OFF duty and any other status this is the reason we started this loop from hosStatusForDay.length()-2
            for (i = currentHosCycleData.length() - 2; i >= 0; i--) {
                hosWeekCycle.put(currentHosCycleData.getJSONObject(i));
            }

            return hosWeekCycle;
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            return null;
        }
    }

    void calculateViolations(JSONArray truncatedHoSWeek) {
        int i = 0;
        long HoSRuleAllowedDrivingSeconds = CONF.HOS_RULE_8_BY_70_HOURS;
        JSONObject hosForDay; // Get hos for single day with all status for that day
        long drivingHoursInSeconds = 0; // Variable to calculate consecutive OFF duty or SB duty duration

        try {

            for (i = 0; i < truncatedHoSWeek.length(); i++) {
                hosForDay = truncatedHoSWeek.getJSONObject(i);
                drivingHoursInSeconds += hosForDay.getLong("drivingTime");
            }

            if (drivingHoursInSeconds > HoSRuleAllowedDrivingSeconds) {
                System.out.println("Driving hour violation " + drivingHoursInSeconds);
            }

            System.out.println("Total Driving in HoS week cycle(Seconds): " + drivingHoursInSeconds);
        } catch (JSONException ex) {
            Logger.getLogger(ProcessEngine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
