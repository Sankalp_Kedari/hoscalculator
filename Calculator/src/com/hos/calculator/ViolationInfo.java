/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hos.calculator;

/**
 *
 * @author skedari
 */
public class ViolationInfo {

    public long createdDate;
    public int violationId;
    public String latitude;
    public String longitude;
    public String locationName;

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public void setViolationId(int violationId) {
        this.violationId = violationId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

}