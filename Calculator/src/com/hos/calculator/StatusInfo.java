/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hos.calculator;

/**
 *
 * @author skedari
 */
public class StatusInfo {

    private long endTimeStamp;
    private String dutyStatus;
    private int eventId;
    private int odometer;
    private int duration;
    private int tetheredToDongle;
    private String latitude = "";
    private String longitude = "";
    private String vin = "";
    private int speed;
    private String comment;
    private String locationName = "";
    private String tractorNumber;
    private String trailerNumber;
    private transient boolean fromServer = false; //set as transient to avoid it from json creation


    public String getTractorNumber() {
        return tractorNumber;
    }

    public void setTractorNumber(String tractorNumber) {
        this.tractorNumber = tractorNumber;
    }

    public String getTrailerNumber() {
        return trailerNumber;
    }

    public void setTrailerNumber(String trailerNumber) {
        this.trailerNumber = trailerNumber;
    }

    public long getEndTimeStamp() {
        return endTimeStamp;
    }

    public void setEndTimeStamp(long endTimeStamp) {
        this.endTimeStamp = endTimeStamp;
    }

    public String getDutyStatus() {
        return dutyStatus;
    }

    public void setDutyStatus(String dutyStatus) {
        this.dutyStatus = dutyStatus;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getTetheredToDongle() {
        return tetheredToDongle;
    }

    public void setTetheredToDongle(int tetheredToDongle) {
        this.tetheredToDongle = tetheredToDongle;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public boolean isFromServer() {
        return fromServer;
    }

    public void setFromServer(boolean fromServer) {
        this.fromServer = fromServer;
    }

    @Override
    public String toString() {
        return "StatusInfo{" +
                "endTimeStamp=" + endTimeStamp +
                ", dutyStatus='" + dutyStatus + '\'' +
                ", eventId=" + eventId +
                ", odometer=" + odometer +
                ", duration=" + duration +
                ", tetheredToDongle=" + tetheredToDongle +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", vin='" + vin + '\'' +
                ", comment='" + comment + '\'' +
                ", locationName='" + locationName + '\'' +
                ", speed=" + speed +
                ", tractorNumber='" + tractorNumber + '\'' +
                ", trailerNumber='" + trailerNumber + '\'' +
                ", fromServer=" + fromServer +
                '}';
    }
}