/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hos.calculator;

import java.util.ArrayList;

/**
 *
 * @author skedari
 */
public class HosInfo {

    public int serverHosLogId;
    public int isLogCertified;
    public long onDutyTime;
    public long drivingTime;
    public long offDutyTime;
    public long sleeperBerthTime;
    public long tripDistance;
    public String certifiedBy;
     private String currentStatus;
    public ArrayList<ViolationInfo> violation;

    private Long createdDate;//set data type as wrapper class instead of long to avoid it from json creation
    private String hosDriverName;
    private String hosDistance;
    private String hosTractorId;
    private String hosTrailerId;
    private String hosCarrierName;
    private String hosOfficeAddress;
    private String hosShippingDocs;
    public ArrayList<StatusInfo> status;
    //private ArrayList<Overview> overviewList;

    public String getStatusHrsAvaToday() {
        return statusHrsAvaToday;
    }

    public void setStatusHrsAvaToday(String statusHrsAvaToday) {
        this.statusHrsAvaToday = statusHrsAvaToday;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public ArrayList<StatusInfo> getStatus() {
        return status;
    }

    public void setStatus(ArrayList<StatusInfo> status) {
        this.status = status;
    }

    private String statusHrsAvaToday;


    public ArrayList<StatusInfo> getStatusList() {
        return status;
    }

    public void setStatusList(ArrayList<StatusInfo> statusList) {
        this.status = statusList;
    }

//    public ArrayList<Overview> getOverviewList() {
//        return overviewList;
//    }
//
//    public void setOverviewList(ArrayList<Overview> overviewList) {
//        this.overviewList = overviewList;
//    }

    public String getHosOfficeAddress() {
        return hosOfficeAddress;
    }

    public void setHosOfficeAddress(String hosOfficeAddress) {
        this.hosOfficeAddress = hosOfficeAddress;
    }

    public String getHosShippingDocs() {
        return hosShippingDocs;
    }

    public void setHosShippingDocs(String hosShippingDocs) {
        this.hosShippingDocs = hosShippingDocs;
    }

    public String getHosCarrierName() {
        return hosCarrierName;
    }

    public void setHosCarrierName(String hosCarrierName) {
        this.hosCarrierName = hosCarrierName;
    }

    public String getHosTractorId() {
        return hosTractorId;
    }

    public void setHosTractorId(String hosTractorId) {
        this.hosTractorId = hosTractorId;
    }

    public String getHosTrailerId() {
        return hosTrailerId;
    }

    public void setHosTrailerId(String hosTrailerId) {
        this.hosTrailerId = hosTrailerId;
    }

    public String getHosDistance() {
        return hosDistance;
    }

    public void setHosDistance(String hosDistance) {
        this.hosDistance = hosDistance;
    }

    public String getHosDriverName() {
        return hosDriverName;
    }

    public void setHosDriverName(String hosDriverName) {
        this.hosDriverName = hosDriverName;
    }

    public String getCertifiedBy() {
        return certifiedBy;
    }

    public void setCertifiedBy(String certifiedBy) {
        this.certifiedBy = certifiedBy;
    }


    public ArrayList<ViolationInfo> getViolation() {
        return violation;
    }

    public void setViolation(ArrayList<ViolationInfo> violation) {
        this.violation = violation;
    }

    public int getServerHosLogId() {
        return serverHosLogId;
    }

    public void setServerHosLogId(int serverHosLogId) {
        this.serverHosLogId = serverHosLogId;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public int isLogCertified() {
        return isLogCertified;
    }

    public void setIsLogCertified(int isLogCertified) {
        this.isLogCertified = isLogCertified;
    }

    public long getOnDutyTime() {
        return onDutyTime;
    }

    public void setOnDutyTime(long onDutyTime) {
        this.onDutyTime = onDutyTime;
    }

    public long getDrivingTime() {
        return drivingTime;
    }

    public void setDrivingTime(long drivingTime) {
        this.drivingTime = drivingTime;
    }

    public long getOffDutyTime() {
        return offDutyTime;
    }

    public void setOffDutyTime(long offDutyTime) {
        this.offDutyTime = offDutyTime;
    }

    public long getSleeperBerthTime() {
        return sleeperBerthTime;
    }

    public void setSleeperBerthTime(long sleeperBerthTime) {
        this.sleeperBerthTime = sleeperBerthTime;
    }

    public long getTripDistance() {
        return tripDistance;
    }

    public void setTripDistance(long tripDistance) {
        this.tripDistance = tripDistance;
    }

    @Override
    public String toString() {
        return "HosInfo{" +
                "serverHosLogId='" + serverHosLogId + '\'' +
                ", createdDate=" + createdDate +
                ", isLogCertified=" + isLogCertified +
                ", onDutyTime=" + onDutyTime +
                ", drivingTime=" + drivingTime +
                ", offDutyTime=" + offDutyTime +
                ", sleeperBerthTime=" + sleeperBerthTime +
                ", tripDistance=" + tripDistance +
                ", certifiedBy='" + certifiedBy + '\'' +
                ", status=" + status +
                ", violation=" + violation +
                '}';
    }
}