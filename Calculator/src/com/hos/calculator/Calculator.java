/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hos.calculator;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextField;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Sankalp
 */
public class Calculator implements Runnable{

    Thread t;
    ProcessEngine engine;
    JSONObject currentCycle;
    long HR_1_CONFIGURABLE_SECONDS;
    long Hr10BreakTimer = 0,Hr34BreakTimer = 0,Min30BreakTimer = 0;
    long total30minBreakTimer = 0 ,total10HrBreakTimer = 0;
    //For UI update
    JTextField breakTimerTextOne,breakTimerTextTwo,breakTimerTextThree;
    JTextField remainingBrakeTimerTextOne, remainingBrakeTimerTextTwo,remainingBrakeTimerTextThree;
    String currentStatus;
    
    
    
    public Calculator(long HR_1_CONFIGURABLE_SECONDS,String currentStatus ,JTextField breakTimerTextOne,JTextField breakTimerTextTwo,JTextField BreakTimerTextThree,
    JTextField remainingBrakeTimerTextOne,JTextField remainingBrakeTimerTextTwo,JTextField remainingBrakeTimerTextThree) throws JSONException {
        this.currentStatus = currentStatus;
        this.breakTimerTextOne = breakTimerTextOne;
        this.breakTimerTextTwo = breakTimerTextTwo;
        this.breakTimerTextThree = BreakTimerTextThree;
        this.remainingBrakeTimerTextOne = remainingBrakeTimerTextOne;
        this.remainingBrakeTimerTextTwo = remainingBrakeTimerTextTwo;
        this.remainingBrakeTimerTextThree = remainingBrakeTimerTextThree;
        this.HR_1_CONFIGURABLE_SECONDS = HR_1_CONFIGURABLE_SECONDS;
        engine = new ProcessEngine();
        generateDatasets();
        t = new Thread(this);
        t.start();
        
    }

    @Override
    public void run() {
        while(Thread.currentThread() == t)
        {
            updateTimer(currentStatus);
            try {
                Thread.sleep(CONSTANTS.MIN_1_RESET_SECONDS * 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public void updateTimer(String status) {
            currentStatus = status;
            System.out.println("Duty Status : " + currentStatus);
            if(currentStatus.equalsIgnoreCase("OFF DUTY") || currentStatus.equalsIgnoreCase("SLEEPER BERTH")) {
                Min30BreakTimer++;
                Hr10BreakTimer++;
                Hr34BreakTimer++;
            } else if(currentStatus.equalsIgnoreCase("ON DUTY")) {
                Min30BreakTimer = 0;
                Hr10BreakTimer = 0;
                Hr34BreakTimer = 0;
            } else if(currentStatus.equalsIgnoreCase("DRIVING")) { 
                Hr10BreakTimer = 0;
                Min30BreakTimer = 0;
                Hr34BreakTimer = 0;
            }
            breakTimerTextOne.setText("" + Min30BreakTimer);
            breakTimerTextTwo.setText("" + Hr10BreakTimer);
            breakTimerTextThree.setText("" + Hr34BreakTimer);
            remainingBrakeTimerTextOne.setText("" + ((CONSTANTS.MIN_30_BREAK_SECONDS/60)-Min30BreakTimer));
            remainingBrakeTimerTextTwo.setText("" + ((CONSTANTS.HRS_10_BREAK_SECONDS/60)-Hr10BreakTimer));
            remainingBrakeTimerTextThree.setText(""+((CONSTANTS.HRS_34_RESET_SECONDS/60)-Hr34BreakTimer));
        }
    
    public  ArrayList<HosInfo> generateDatasets() throws JSONException
    {
        try{
        JSONArray hosWeekCycle;
        JSONArray truncatedHoSWeek;
        hosWeekCycle = new JSONArray("[{\"serverHosLogId\":151,\"createdDate\":1454047245557,\"isLogCertified\":1,\"certifiedBy\":\"QaAk\",\"onDutyTime\":3580,\"drivingTime\":32401,\"offDutyTime\":21622,\"sleeperBerthTime\":28793,\"currentStatus\":\" \",\"tripDistance\":0.0,\"status\":[{\"endTimeStamp\":1454068822982,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":21622.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454072403802,\"dutyStatus\":\"ON DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":3580.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454090406547,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":18002.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454092204580,\"dutyStatus\":\"SLEEPER BERTH\",\"eventId\":0,\"odometer\":0,\"duration\":1798.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454106604168,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":14399.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454133599999,\"dutyStatus\":\"SLEEPER BERTH\",\"eventId\":0,\"odometer\":0,\"duration\":26995.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null}],\"violation\":[]},{\"serverHosLogId\":152,\"createdDate\":1454133620706,\"isLogCertified\":0,\"certifiedBy\":null,\"onDutyTime\":3599,\"drivingTime\":32400,\"offDutyTime\":28795,\"sleeperBerthTime\":21604,\"currentStatus\":\" \",\"tripDistance\":0.0,\"status\":[{\"endTimeStamp\":1454155204889,\"dutyStatus\":\"SLEEPER BERTH\",\"eventId\":0,\"odometer\":0,\"duration\":21604.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454158804355,\"dutyStatus\":\"ON DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":3599.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454176803629,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":17999.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454178603674,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":1800.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454193004822,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":14401.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454219999999,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":26995.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null}],\"violation\":[]},{\"serverHosLogId\":153,\"createdDate\":1454220019180,\"isLogCertified\":0,\"certifiedBy\":null,\"onDutyTime\":3598,\"drivingTime\":32402,\"offDutyTime\":50397,\"sleeperBerthTime\":0,\"currentStatus\":\" \",\"tripDistance\":0.0,\"status\":[{\"endTimeStamp\":1454306399999,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":86400,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null}],\"violation\":[]},{\"serverHosLogId\":154,\"createdDate\":1454306420362,\"isLogCertified\":0,\"certifiedBy\":null,\"onDutyTime\":3597,\"drivingTime\":32380,\"offDutyTime\":21605,\"sleeperBerthTime\":28816,\"currentStatus\":\" \",\"tripDistance\":0.0,\"status\":[{\"endTimeStamp\":1454349604502,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":21605.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454351428715,\"dutyStatus\":\"ON DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":1824.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454365807822,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":14379.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454392799999,\"dutyStatus\":\"SLEEPER BERTH\",\"eventId\":0,\"odometer\":0,\"duration\":26992.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null}],\"violation\":[]},{\"serverHosLogId\":155,\"createdDate\":1454392822592,\"isLogCertified\":0,\"certifiedBy\":null,\"onDutyTime\":3599,\"drivingTime\":32383,\"offDutyTime\":28811,\"sleeperBerthTime\":21604,\"currentStatus\":\" \",\"tripDistance\":0.0,\"status\":[{\"endTimeStamp\":1454414404542,\"dutyStatus\":\"SLEEPER BERTH\",\"eventId\":0,\"odometer\":0,\"duration\":21604.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454418003726,\"dutyStatus\":\"ON DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":3599.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454436004419,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":18000.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454437821521,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":1817.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454452205146,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":14383.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454479199999,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":26994.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null}],\"violation\":[{\"createdDate\":1454452205146,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454479199999,\"violationId\":60,\"latitude\":0.0,\"longitude\":0.0,\"locationName\":\"\"}]},{\"serverHosLogId\":156,\"createdDate\":1454479240037,\"isLogCertified\":0,\"certifiedBy\":null,\"onDutyTime\":3600,\"drivingTime\":32400,\"offDutyTime\":36000,\"sleeperBerthTime\":14396,\"currentStatus\":\" \",\"tripDistance\":0.0,\"status\":[{\"endTimeStamp\":1454500804931,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":21604.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454504405296,\"dutyStatus\":\"ON DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":3600.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454522404937,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":17999.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454524203766,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":1798.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454538605105,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":14401.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454551203483,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":12598.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454565599999,\"dutyStatus\":\"SLEEPER BERTH\",\"eventId\":0,\"odometer\":0,\"duration\":14396.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null}],\"violation\":[{\"createdDate\":1454500804931,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454504405296,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454522404937,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454524203766,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454538605105,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454538605105,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454538605105,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454538605105,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454551203483,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454551203483,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454551203483,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454551203483,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454565599999,\"violationId\":11,\"latitude\":0.0,\"longitude\":0.0,\"locationName\":\"\"},{\"createdDate\":1454565599999,\"violationId\":14,\"latitude\":0.0,\"longitude\":0.0,\"locationName\":\"\"},{\"createdDate\":1454565599999,\"violationId\":60,\"latitude\":0.0,\"longitude\":0.0,\"locationName\":\"\"},{\"createdDate\":1454565599999,\"violationId\":70,\"latitude\":0.0,\"longitude\":0.0,\"locationName\":\"\"},{\"createdDate\":1454500804931,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454504405296,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454522404937,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454524203766,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454538605105,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454538605105,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454538605105,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454538605105,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454551203483,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454551203483,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454551203483,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454551203483,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454565599999,\"violationId\":11,\"latitude\":0.0,\"longitude\":0.0,\"locationName\":\"\"},{\"createdDate\":1454565599999,\"violationId\":14,\"latitude\":0.0,\"longitude\":0.0,\"locationName\":\"\"},{\"createdDate\":1454565599999,\"violationId\":60,\"latitude\":0.0,\"longitude\":0.0,\"locationName\":\"\"},{\"createdDate\":1454565599999,\"violationId\":70,\"latitude\":0.0,\"longitude\":0.0,\"locationName\":\"\"}]},{\"serverHosLogId\":157,\"createdDate\":1454565628189,\"isLogCertified\":0,\"certifiedBy\":null,\"onDutyTime\":3582,\"drivingTime\":32400,\"offDutyTime\":23401,\"sleeperBerthTime\":21620,\"currentStatus\":\"SLEEPER BERTH\",\"tripDistance\":0.0,\"status\":[{\"endTimeStamp\":1454587220429,\"dutyStatus\":\"SLEEPER BERTH\",\"eventId\":0,\"odometer\":0,\"duration\":21620.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454590803304,\"dutyStatus\":\"ON DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":3582.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454608804400,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":18001.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454610604882,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":1800.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454625004196,\"dutyStatus\":\"DRIVING\",\"eventId\":0,\"odometer\":0,\"duration\":14399.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null},{\"endTimeStamp\":1454646605474,\"dutyStatus\":\"OFF DUTY\",\"eventId\":0,\"odometer\":0,\"duration\":21601.0,\"tetheredToDongle\":0,\"latitude\":0.0,\"longitude\":0.0,\"vin\":null,\"comment\":null,\"locationName\":\"\",\"speed\":0,\"tractorNumber\":null,\"trailerNumber\":null}],\"violation\":[{\"createdDate\":1454587220429,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454587220429,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454587220429,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454587220429,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454590803304,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454590803304,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454590803304,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454590803304,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454608804400,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454608804400,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454608804400,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454608804400,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454610604882,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454610604882,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454610604882,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454610604882,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454625004196,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454625004196,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454625004196,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454625004196,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454646605474,\"violationId\":11,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454646605474,\"violationId\":14,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454646605474,\"violationId\":60,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"},{\"createdDate\":1454646605474,\"violationId\":70,\"latitude\":-200.0,\"longitude\":-200.0,\"locationName\":\"\"}]}]");
        // Truncate hosWeekCycle to detect 34 hour reset cycle and remove unwanted HoS log
        truncatedHoSWeek = engine.truncate34HrsResetCycle(hosWeekCycle);
        
        
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<HosInfo> hosInfoList = new ArrayList<HosInfo>();
        
         hosInfoList = mapper.readValue(truncatedHoSWeek.toString(),
                 TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, HosInfo.class));
         
        for(HosInfo hosInfo : hosInfoList)
        System.out.println("current cycle:" + hosInfo);
        
        return hosInfoList;
        
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
  
}
